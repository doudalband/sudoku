﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUDOKU
{
    internal class Grille
    {

        private List<List<Region>>? grille;

        public Grille() {
            this.grille = initGrille();
        }

        public List<List<Region>> initGrille()
        {
            List<List<Region>> listeRegion = new List<List<Region>>();
            for (int i = 0; i < 3; i++)
            {
                List<Region> listeLigne = new List<Region>();
                for (int y = 0; y < 3; y++)
                {
                    Region regionGrille = new Region(i, y);
                    listeLigne.Add(regionGrille);
                }
                listeRegion.Add(listeLigne);
            }
            return listeRegion;
        }

        public void rempliGrille()
        {
            for (int i = 0; i < 3; i++)
            {
                List<int> listeRegion1 = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                List<int> listeRegion2 = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                List<int> listeRegion3 = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
                for (int j = 0; j < 3; j++)
                {
                    List<int> listeChiffreLigne = new List<int>() { 1,2,3,4,5,6,7,8,9};
                    for (int k = 0; k < 3; k++)
                    {             
                        for (int y = 0; y < 3; y++)
                        {
                            Random chiffreRandom = new Random();
                            int indice = chiffreRandom.Next(listeChiffreLigne.Count);
                            int nb = listeChiffreLigne[indice];
                            
                            listeChiffreLigne.RemoveAt(indice);
                            this.grille[i][k].getCase(j, y).setChiffre(nb);
                            switch (j) { 
                                case 1:

                                    break;
                                case 2:
                                    break;
                                case 3:
                                    
                                    break;
                            }
                        }
                    }
                }
            }
        }

        public override string ToString()
        {
            StringBuilder affichage = new StringBuilder("\n");
            affichage.Append("┌");
            for (int colonne = 0; colonne < 8; ++colonne)
            {
                affichage.Append("───┬");
            }
            affichage.Append("───┐" + "\n");
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (i != 0 || i % 3 == 0)
                    {
                        affichage.Append("│");
                    }
                    for (int k = 0; k < 3; k++)
                    {
                        for (int y = 0; y < 3; y++)
                        {
                            Case? c = this.grille[i][k].getCase(j, y);
                            if(c.getChiffre() != null)
                            {
                                affichage.Append(" " +c.getChiffre().ToString() + " │");
                            }
                            else
                            {
                                affichage.Append("   │");
                            }
                        }
                    }
                    affichage.Append('\n');
                    if (i == 2 && j==2)
                    {
                        affichage.Append("└");
                        for (int x = 0; x < 8; ++x)
                        {
                            affichage.Append("───┴");
                        }
                        affichage.Append("───┘");
                    }
                    else
                    {
                        affichage.Append("├");
                        for (int p = 0; p < 8; ++p)
                        {
                            affichage.Append("───┼");
                        }
                        affichage.Append("───┤\n");
                    }
                }
            }
                return affichage.ToString();
        }



    }
}
