﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUDOKU
{
    internal class Region
    {

        private int ligne { get; set; }
        private int colonne { get; set; }
        private List<List<Case>>? region { get; set; }

        public Region(int ligne, int colonne) { 
            this.ligne = ligne;
            this.colonne = colonne;
            this.region = initRegion();

        }

        private List<List<Case>> initRegion()
        {
            List<List<Case>> listeCase = new List<List<Case>>();
            List<int> maListe = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            for (int i = 0; i < 3; i++)
            {
                List<Case> listeLigne = new List<Case>();
                for (int y = 0; y < 3; y++)
                {
                    Case caseRegion = new Case(i, y, null);
                    listeLigne.Add(caseRegion);
                }
                listeCase.Add(listeLigne);
            }
            return listeCase;
        }

        public Case? getCase(int ligne, int colonne)
        {
            if (this.region[ligne][colonne] is not null)
            {
                return this.region[ligne][colonne];
            }
            else
            {
                return null;
            }
        }

        public List<int?> getLigne(int ligne)
        {
            List<int?> set = new List<int?>();
            for(int colonne = 0; colonne <3; colonne++)
            {
                if(getCase(ligne, colonne) != null)
                {
                    set.Add(getCase(ligne, colonne).getChiffre());
                }
            }
            return set;
        }

        public List<int?> getColonne(int colonne)
        {
            List<int?> set = new List<int?>();
            for (int ligne = 0; ligne < 3; ligne++)
            {
                if (getCase(ligne, colonne) != null)
                {
                    set.Add(getCase(ligne, colonne).getChiffre());
                }
            }
            return set;
        }

        public String afficheColonne(int colonne)
        {
            List<int?> listeColonne = getColonne(colonne);
            String res = "";
            for(int i =0; i<3; i++)
            {
                if (listeColonne[i] != null){
                    res += listeColonne[i];
                }
                else
                {
                    res += " ";
                }
            }
            return res;
        }

        public String afficheLigne(int ligne)
        {
            List<int?> listeLigne = getLigne(ligne);
            String res = "";
            for (int i = 0; i < 3; i++)
            {
                if (listeLigne[i] != null)
                {
                    res += listeLigne[i];
                }
                else
                {
                    res += " ";
                }
            }
            return res;
        }

        public IEnumerator GetEnumerator()
        {
            // Ici, vous devez retourner un itérateur pour parcourir les éléments de la région
            // Par exemple, si vous avez une liste d'éléments dans la région, vous pouvez retourner son itérateur
            return this.region.GetEnumerator();
        }

        public override String ToString()
        {
            return base.ToString();

        }

        /*String affichage = "";
            affichage +=  "   ┌";
            for (int colonne = 0; colonne < 2; ++colonne)
            {
                affichage += "───┬";
            }
            affichage += "───┐" + "\n";
            for (int ligne = 0; ligne < 3; ++ligne)
            {
                affichage += "   │";
                for (int colonne = 0; colonne < 3; ++colonne)
                {
                    if (getCase(ligne, colonne).getChiffre() != null)
                    {
                        affichage += " " + getCase(ligne, colonne).getChiffre() + " │";
                    }
                    else
                    {
                        affichage += "   │";
                    }
                }
                affichage += "\n";
                if (ligne == 2)
                {
                    affichage += "   └";
                    for (int i = 0; i < 2; ++i)
                    {
                        affichage += "───┴";
                    }
                    affichage += "───┘";
                }
                else
                {
                    affichage += "   ├";
                    for (int i = 0; i < 2; ++i)
                    {
                        affichage += "───┼";
                    }
                    affichage += "───┤\n";
                }
            }
            return affichage;*/
    }
}
