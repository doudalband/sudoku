﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SUDOKU
{

    internal class Case
    {

        private int ligne { get; set; }
        private int colonne { get; set; }
        private int? chiffre { get; set; }


        public Case(int ligne, int colonne, int? chiffre){
            this.ligne = ligne;
            this.colonne = colonne;
            this.chiffre = chiffre;
        }

        public void setChiffre(int chiffre)
        {
            this.chiffre = chiffre;
        }

        public int? getChiffre()
        {
            return this.chiffre;
        }

        public override string? ToString()
        {
            return base.ToString();
        }
    }
}
